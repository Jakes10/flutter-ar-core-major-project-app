import 'package:flutter/material.dart';
import 'package:sculpturepark/screens/home_screen.dart';
import 'package:sculpturepark/screens/splashScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    // status bar color
    return MaterialApp(

      debugShowCheckedModeBanner: false,

      title: 'Sculpture Park',


      theme: ThemeData(
        primarySwatch: Colors.blue,

      ),
//      home: HomeScreen(),
      home: SplashScreen(),
    );
  }
}
