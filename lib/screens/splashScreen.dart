import 'package:flutter/material.dart';

import 'home_screen.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Container(
        child: new DecoratedBox(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/images/background.jpg"),
              fit: BoxFit.fill,

            ),
          ),
          child: Center(
            child: Padding(
              padding: EdgeInsets.fromLTRB(40, 380, 40, 10),
              child: Container(
                width: 300,
                child: Material(
                  borderRadius: BorderRadius.circular(15),
                  shadowColor: Colors.black,
                  elevation: 5,
                  color: Colors.green,
                  child: MaterialButton(
                    height: 80,

                    onPressed: (){
                      var route= new MaterialPageRoute(
                          builder: (BuildContext context)=>
                          new HomeScreen()
                      );
                      Navigator.of(context).push(route);
//                    setState(() {
//                      _autoValidate=true;
//                      if(formKey.currentState.validate()){
//                        if(! priceController.text.contains("\$"))
//                          priceController.text= '\$'+priceController.text;
//
//                        if(priceController.text.substring(priceController.text.length-1)==".")//
//                          priceController.text= priceController.text.substring(0, priceController.text.length-1);
//
////                          print(priceController.text.substring(0, priceController.text.length-1));
//
//                        FocusScope.of(context).unfocus();//Remove onscreen keyboard
////                        priceController.clear();
////                        print(priceController.text);
//                        //Execute transaction
//                        generateTag();
//                      }
//                    });


                    },
                    color: Colors.deepOrange,
                    child: Text('GET STARTED', style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.w400 )),
                  ),
                ),
              ),
            ),
//            children: <Widget>[
//              ,
//              Padding(
//                padding: EdgeInsets.fromLTRB(40, 10, 40, 20),
//                child: Material(
//                  borderRadius: BorderRadius.circular(15),
//                  shadowColor: Colors.black,
//                  elevation: 5,
//                  color: Colors.green[500],
//                  child: MaterialButton(
//                    height: 60,
//
//                    onPressed: (){
//
////                      var route= new MaterialPageRoute(
////                          builder: (BuildContext context)=>
////                          new PatientProfile()
////                      );
////                      Navigator.of(context).push(route);
//
//                    },
//                    color: Colors.grey[250],
//                    child: Text('Login', style: TextStyle(color: Colors.white, fontSize: 20 )),
//                  ),
//                ),
//              ),


          ),
        ),

      ),
      

    );
  }
}
