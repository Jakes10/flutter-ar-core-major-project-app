import 'dart:typed_data';

import 'package:flutter/material.dart';
//import 'package:sculpturepark/screens/ChewieListItem.dart';
import 'package:video_player/video_player.dart';
import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:sculpturepark/screens/ChewieListItem.dart';
import 'package:vector_math/vector_math_64.dart' as vector;
import 'package:arkit_plugin/arkit_plugin.dart';

class Animate extends StatefulWidget {

  final String videoUrl;
  Animate({Key key,  this.videoUrl}): super(key: key);
  @override
  _AnimateState createState() => _AnimateState();
}

class _AnimateState extends State<Animate> {
  ArCoreController arCoreController;
//  ARKitController arCoreController;

  _onArCoreViewCreated(ArCoreController _arcoreController){
    arCoreController = _arcoreController;
    _addSphere(arCoreController);
  }
  onArkitViewCreated(ARKitController c){

    final node = ARKitNode(
      geometry:ARKitSphere(
        radius: 1,
        materials: [ARKitMaterial(
          diffuse: ARKitMaterialProperty(image: 'assets/images/tomorrow.jpg')
        )]
      ),
      position: vector.Vector3.zero(),
    );
    c.add(node);
  }

  _addSphere(ArCoreController _arcoreController){
    final material = ArCoreMaterial(color: Colors.blue);
    final sphere = ArCoreSphere(materials: [material], radius: 0.2);
    final node = ArCoreNode(shape: sphere,
        position: vector.Vector3(
            0,
            0,
            -1)
    );


    _arcoreController.addArCoreNode(node);

  }

  @override
  void dispose(){
    arCoreController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:
//        ARKitSceneView(
//          onARKitViewCreated: (c) => onArkitViewCreated(c),
//        )
//      body: ArCoreView(
//          onArCoreViewCreated: _onArCoreViewCreated,
//        )
      ListView(
        children: <Widget>[
          SizedBox(height: 10.0),
          ChewieListItem(
            videoPlayerController: VideoPlayerController.asset(
              widget.videoUrl,
//              "videos/tomorrow.mp4",
            ),
            looping: true,
          ),
//          ChewieListItem(
//            videoPlayerController: VideoPlayerController.network(
////              'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
//              'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
//            ),
//          ),
//          ChewieListItem(
//            // This URL doesn't exist - will display an error
//            videoPlayerController: VideoPlayerController.asset(
//              'videos/aniversary1.mp4',
//            ),
//          ),
        ],
      )
    );
  }
}
