import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sculpturepark/models/sculpture_model.dart';
import 'package:sculpturepark/screens/sculpture_screen.dart';
//import 'package:sculpturepark/models/sculpture_model.dart';
//import 'package:sculpturepark/screens/ChewieListItem.dart';
//import 'package:video_player/video_player.dart';
//
//import 'sculpture_screen.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8);
  }



  _sculptureSelector(int index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 270.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: GestureDetector(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => SculptureScreen(sculpture: sculptures[index]),
          ),
        ),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black54,
                      offset: Offset(0.0, 4.0),
                      blurRadius: 10.0,
                    ),
                  ],
                ),
                child: Center(
                  child: Hero(
                    tag: sculptures[index].imageUrl,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Image(
                        image: AssetImage(sculptures[index].imageUrl),
                        height: 220.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 30.0,
              bottom: 40.0,
              child: Container(
                width: 250.0,
                child: Text(
                  sculptures[index].title.toUpperCase(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w900,

                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Image(
            image: AssetImage('assets/images/park_logo.png'),
          ),
          leading: IconButton(
            padding: EdgeInsets.only(left: 30.0),
            onPressed: () => print('Menu'),
            icon: Icon(Icons.menu),
            iconSize: 30.0,
            color: Colors.black,
          ),
          actions: <Widget>[
            IconButton(
              padding: EdgeInsets.only(right: 30.0),
              onPressed: () => print('Search'),
              icon: Icon(Icons.search),
              iconSize: 30.0,
              color: Colors.black,
            ),
          ],
        ),
        body: ListView(
          children: <Widget>[
//          Container(
//            height: 280.0,
//            width: double.infinity,
//            child: PageView.builder(
//              controller: _pageController,
//              itemCount: sculptures.length,
//              itemBuilder: (BuildContext context, int index) {
//                return _sculptureSelector(index);
//              },
//            ),
//          ),
//          SizedBox(height: 10.0),
//          ChewieListItem(
//            videoPlayerController: VideoPlayerController.asset(
//              'videos/Core.mp4',
//            ),
//            looping: true,
//          ),
//          ChewieListItem(
//            videoPlayerController: VideoPlayerController.network(
////              'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
//              'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
//            ),
//          ),
//          ChewieListItem(
//            // This URL doesn't exist - will display an error
//            videoPlayerController: VideoPlayerController.network(
//              'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/error.mp4',
//            ),
//          ),


          Container(
          height: 280.0,
          width: double.infinity,
          child: PageView.builder(
            controller: _pageController,
            itemCount: sculptures.length,
            itemBuilder: (BuildContext context, int index) {
              return _sculptureSelector(index);
            },
          ),
        ),
        SizedBox(height: 10.0),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
//                Container(
//                  decoration: new BoxDecoration(
////                      color: Colors.green, //new Color.fromRGBO(255, 0, 0, 0.0),
//                      borderRadius: new BorderRadius.all( const  Radius.circular(60.0))
//                  ),
//                  height: 180,
//                  width: double.infinity,
//                  child: Card(
//
//                    elevation: 6,
////                    color: Colors.pink,
//                    child: Padding(
//                      padding: const EdgeInsets.all(16.0),
//                    ),
//                  ),
//                ),
              Divider(color: Colors.black,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.location_on,
                            color: Colors.deepOrange,
                            size: 36.0,
                          ),
                          Text(
                            'Address',
                            style: TextStyle(fontSize: 20,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5,),
                      Text('237 W.I, 6 Old Hope Rd,', style: TextStyle(fontSize: 18)),
                      Text('Kingston', style: TextStyle(fontSize: 18)),
//                              Row(
//                                children: <Widget>[
//                                  new FlatButton(
//                                    child: new Text("Button text"),
//                                        onPressed: null,
//                                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
//                                    )
//                                ],
//                              )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: Colors.red)),
                        color: Colors.white,
                        textColor: Colors.red,
                        padding: EdgeInsets.all(8.0),
                        onPressed: () {},
                        child: Text(
                          "Website".toUpperCase(),
                          style: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: Colors.red)),
                        color: Colors.white,
                        textColor: Colors.red,
                        padding: EdgeInsets.all(8.0),
                        onPressed: () {},
                        child: Text(
                          "Locate".toUpperCase(),
                          style: TextStyle(
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),

              Divider(color: Colors.black ),
              SizedBox(height: 15.0),

              Text(
                "Caribbean Sculpture Park".toUpperCase(),
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 10.0),
              Text(
                "Located at the University’s Papine campus and the only open-air museum of its kind in the English-speaking Caribbean, the Caribbean Sculpture Park facilitates the sensitization of the UTech, Jamaica community to the arts.",

                style: TextStyle(

                  color: Colors.black45,
                ),
              ),
              SizedBox(height: 15.0),

              Text(
                "Sculpture Pieces".toUpperCase(),
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 10.0),
              Text(
                "The sculptures are of life size and are done to scale in harmony with the physical and natural environment. The primary focus of the Park is on the artistic expression, vision and cultural contribution of each piece to humanity, linking the Arts with Technology. The material used for each sculpture is not only artistic but also durable, therefore evolving with age.",
                style: TextStyle(

                  color: Colors.black45,
                ),
              ),
              SizedBox(height: 15.0),


            ],
          ),
        )
    ])
    );
  }
}
