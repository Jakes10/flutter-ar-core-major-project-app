import 'package:flutter/material.dart';
//import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
//import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:sculpturepark/models/sculpture_model.dart';
import 'package:sculpturepark/screens/Animation.dart';
import 'package:sculpturepark/screens/qrScanScreen.dart';
import 'package:sculpturepark/widgets/circular_clipper.dart';
import 'package:sculpturepark/widgets/content_scroll.dart';


class SculptureScreen extends StatefulWidget {
  final Sculpture sculpture;

  SculptureScreen({this.sculpture});

  @override
  _SculptureScreenState createState() => _SculptureScreenState();
}

class _SculptureScreenState extends State<SculptureScreen> {

  GlobalKey qrkey = GlobalKey();
//  QRViewController controller;
  bool _like=false;

  Future scanCode() async {

//    await FlutterBarcodeScanner.scanBarcode("#004297" , "Cancel", true);


  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[

              Container(
                transform: Matrix4.translationValues(0.0, -50.0, 0.0),
                child: Hero(
                  tag: widget.sculpture.imageUrl,
                  child: ClipShadowPath(
                    clipper: CircularClipper(),
                    shadow: Shadow(blurRadius: 20.0),
                    child: Image(
                      height: 400.0,
                      width: double.infinity,
                      fit: BoxFit.cover,
                      image: AssetImage(widget.sculpture.imageUrl),
                    ),
                  ),
                ),
              )
              ,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    padding: EdgeInsets.only(left: 30.0),
                    onPressed: () => Navigator.pop(context),
                    icon: Icon(Icons.arrow_back),
                    iconSize: 30.0,
                    color: Colors.white,
                  ),
//                  Image(
//                    image: AssetImage('assets/images/park_logo.png'),
//                    height: 60.0,
//                    width: 150.0,
//                  ),
                  IconButton(
                    padding: EdgeInsets.only(left: 30.0),
                    onPressed: () {
                      setState(() {

                        if (_like)
                          _like=false;
                        else
                          _like=true;


                      });
                    },
                    icon: new Icon(
//                        Icons.favorite_border
                      _like ? Icons.favorite : Icons.favorite_border,
                      color:  _like ? Colors.red : null,
                    ),
                    iconSize: 30.0,
                    color: Colors.white,
                  ),
                ],
              ),

              Positioned.fill(
                bottom: 10.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,

                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: RawMaterialButton(

                            padding: EdgeInsets.all(10.0),
                            elevation: 12.0,
                            onPressed: (){
                              var route= new MaterialPageRoute(
                                  builder: (BuildContext context)=>
                                      new Animate(videoUrl: widget.sculpture.videoUrl,)
//                                  new Animation()
                              );
                              Navigator.of(context).push(route);
                            },
                            shape: CircleBorder(),
                            fillColor: Colors.white,
                            child: Icon(
                              Icons.play_arrow,
                              size: 60.0,
                              color: Colors.red,
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: RawMaterialButton(

                            padding: EdgeInsets.all(15.0),
                            elevation: 12.0,
                            onPressed: (){
                              var route= new MaterialPageRoute(
                                  builder: (BuildContext context)=>
                                  new QrScan()
                              );
                              Navigator.of(context).push(route);
                            },
                            shape: CircleBorder(),
                            fillColor: Colors.white,
                            child: Icon(
                              Icons.photo_camera,
                              size: 50.0,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topCenter,
                          child: Text(
                            "Animate",
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(width: 25),
                        GestureDetector(
                          onTap: (){
                            scanCode();
                          },
                          child: Align(
                            widthFactor:1.8,
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Scan",
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
//              Positioned(
//                bottom: 0.0,
//                left: 20.0,
//                child: IconButton(
//                  onPressed: () => print('Add to My List'),
//                  icon: Icon(Icons.add),
//                  iconSize: 40.0,
//                  color: Colors.black,
//                ),
//              ),
//              Positioned(
//                bottom: 0.0,
//                right: 25.0,
//                child: IconButton(
//                  onPressed: () => print('Share'),
//                  icon: Icon(Icons.share),
//                  iconSize: 35.0,
//                  color: Colors.black,
//                ),
//              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 20.0),

                Text(
                  widget.sculpture.title.toUpperCase(),
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10.0),
                Text(
                  widget.sculpture.artist,
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 12.0),
                Text(
                  '⭐ ⭐ ⭐ ⭐',
                  style: TextStyle(fontSize: 25.0),
                ),
                SizedBox(height: 15.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          'Faculty',
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 2.0),
                        Text(
                          widget.sculpture.faculty.toString(),
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          'Nationality',
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 2.0),
                        Text(
                          widget.sculpture.country.toUpperCase(),
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          'Medium',
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 2.0),
                        Text(
                          '${widget.sculpture.medium} ',
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 25.0),
                Container(
//                  height: 100.0,
                  child: SingleChildScrollView(
                    child: Text(
                      widget.sculpture.description,

                      style: TextStyle(
                        color: Colors.black54,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),

          ContentScroll(
            images: widget.sculpture.screenshots,
            title: 'Screenshots',
            imageHeight: 200.0,
            imageWidth: 250.0,
          ),
        ],
      ),
    );
  }
}
