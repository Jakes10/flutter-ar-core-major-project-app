import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';

class QrScan extends StatefulWidget {
  @override
  _QrScanState createState() => _QrScanState();
}

class _QrScanState extends State<QrScan> {
  GlobalKey qrkey = GlobalKey();
  QRViewController controller;
  var qrText;

  @override
  void dispose(){
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(

              overlay: QrScannerOverlayShape(
                borderColor: Colors.blue[900],
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 300
              ),
              key: qrkey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
//             child: Text("Result $qrText" ),
             child: Text("Scan QR Code on the sculpture", style: TextStyle(fontSize: 18,color: Colors.blue[900], fontWeight: FontWeight.w900), ),
            ),
          ),
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData){
      setState(() {
        qrText= scanData;

      });
    });
  }
}
