class Sculpture {
  String imageUrl;
  String videoUrl;
  String title;
  String faculty;
  String artist;
  String medium;
  String country;
  String description;
  List<String> screenshots;

  Sculpture({
    this.imageUrl,
    this.videoUrl,
    this.title,
    this.faculty,
    this.artist,
    this.medium,
    this.country,
    this.description,
    this.screenshots,
  });
}

final List<Sculpture> sculptures = [
  //Tomorrow
  Sculpture(
    imageUrl: 'assets/images/tomorrow.jpg',
    videoUrl: 'videos/tomorrow.mp4',
    title: 'Tomorrow',
    faculty: 'N/A',
    artist: 'Edna Manley',
    medium: "Bronze",
    country: "Jamaican",
    description:
    'Edna Manley\'s “Tomorrow” was created in 1938. The sculpture features a lifted head, raised eyes and open hands thrusted upward as if in heavenly praise and need of divine guidance. It points to the recommended path to the future for the Jamaican \"new negro\" who was politically aroused by the labour struggles of that year.',
    screenshots: [
      'assets/images/tomorrow0.jpg',
      'assets/images/tomorrow1.jpg',
      'assets/images/tomorrow2.jpg',
    ],
  ),

  //Untitled
  Sculpture(
    imageUrl: 'assets/images/untitled.jpg',
    title: 'Untitled',
    faculty: 'N/A',
    artist: "Ken Morris",
    medium: "Copper",
    country: "Trinidadian",
    description:
    'This sculpture was a donation by the Jamaica Mutual Life Assurance Society to the Caribbean Sculpture Park as the Park was seen as the most appropriate location for it to be erected both for aesthetic and educational purposes. Also, it was viewed as a good way to complement and continue the Caribbean theme that the Park celebrates.',
    screenshots: [
      'assets/images/untitled0.jpg',

    ],
  ),

  //Earth to Earth
  Sculpture(
    imageUrl: 'assets/images/earth.jpg',
    title: 'Earth to Earth',
    faculty: 'COHS',
    artist: 'Laura Facey',
    country: "Jamaican",
    medium: "Copper",
    description:
    '\“Health is freedom\” - the freedom to develop our full potential. Good health comes when we make a mind-body connection. When we choose positive thoughts, nourishing food and a balanced lifestyle, the body thrives with abundance. Feed it sad, anxious thoughts, it withers and dies. The body is an extraordinary pharmacy with an infinite ability to heal itself if only we allow. '
        'Having completed the design of Earth to Earth, Facey contemplated its meaning. Earth to Earth can be seen as a Yoga position. Yoga is a way of living that brings excellent health through gentle exercise and meditation. '
        'This pose also appears on walls of the Red Chapel of Queen Hatshepsut in Egypt circa 1500BC and has been said to portray an ecstatic dancer. This is exactly what happens when one is in perfect health. There is dancing in the soul, ecstasy in the heart, and joy in the celebration of life.\”',
    screenshots: [
      'assets/images/earth0.jpg',
      'assets/images/earth1.jpg',
    ],
  ),

  //Equus
  Sculpture(
    imageUrl: 'assets/images/equus.jpg',
    title: 'Equus',
    faculty: 'N/A',
    artist: 'Dr. Lance Bannister',
    country: "Barbadian",
    medium: "Welded Car Parts",
    description:
    'The artist describes Equus as his attempt to “convert discarded material mainly from automobiles into works of art”. '
        'Accordingly, he has maintained the appearance and function of each of the automobile parts that make up the sculpture, for e.g. a piston looks like a hoof; in a car it creates power, in the horse it symbolizes the power from the horse to the road. '
        'The horse is represented in a leaping position and the artist says this signifies that as technology increases, we the members of the university community, the country and the world will develop along with it. It has also been specially constructed to allow the horse to rotate 360 degrees. '
        'The artist‘s intent is that the piece “will serve as an educational tool and inspiration to others especially the youth”.',
    screenshots: [
      'assets/images/equus0.jpg',
      'assets/images/equus2.jpg',
      'assets/images/equus1.jpg',
    ],
  ),

////Pictures dont match

  //Icon Of Togetherness
  Sculpture(
    imageUrl: 'assets/images/together0.jpg',
    title: 'Icon Of Togetherness',
    faculty: 'COBAM',
    artist: 'Christopher Gonzalez',
    country: "Jamaican",
    medium: "Copper",
    description:
    'The basic concept of the sculpture is utilizing the specialization areas within the college. The piece is comprised of cactus-like forms moving upward and coming together at the top, with bulb-like forms at the upper ends of the cactus. These are symbolic of the areas of specialization and also provide accent and drama to the work. '
        'At the apex of the piece is a seed-like form which represents the seed of knowledge given to the student by the University. At the other end directly below is a larger seed-like form – this is symbolic of the developed student, wholesome and educated in the field of Business Management. '
        'The sculpture is large enough to accommodate one or two persons to sit on the larger seed on the flooring of the sculpture and have physical interaction with the work. The forms of sculpture should create interesting acoustic reverberation as one sits on the large seed and plays an instrument like a drum or a flute or even sing.',
    screenshots: [
    'assets/images/together.jpg',
    'assets/images/together1.jpg',

    ],
  ),

  //Free Thought
  Sculpture(
    imageUrl: 'assets/images/free.jpg',
    title: 'Free Thought',
    faculty: 'N/A',
    artist: 'Professor Manuel Mendive',
    country: "Cuban",
    medium: "Mixed Media",
    description:
    'Free thought was created by a revered Cuban artist, Professor Manuel Mendive. The work is informed by the Yoruba religion as well as Catholicism. His art often involves an intertwining of flora, fauna and people. '
        'The sculpture features birds (Messengers from and to God), coming out of the heads of the people. It depicts people opening their minds and letting their thoughts fly. The additional chair is to allow the viewer to participate in the sculpture. '
        'The sculpture’s donation to the university was facilitated by Professor James Early of the Folk Museum at the Smithsonian. Early explained that when he was first invited to the Caribbean Sculpture Park in 2001 he realized that Mendive’s work would be suited to the space.',
    screenshots: [
      'assets/images/free.jpg',
      'assets/images/free0.jpg',
      'assets/images/free1.jpg',
    ],
  ),

  //Embracing the Universe
  Sculpture(
    imageUrl: 'assets/images/embrace.jpg',
    title: 'Embracing the Universe',
    faculty: 'N/A',
    artist: 'Stephan Clarke',
    country: "Jamaican",
    medium: "Welded Steel",
    description:
    'The addition of the sculpture, “Embracing the Universe” to the Caribbean Sculpture Park is Simon Frederick’s way of continuing UTech, Jamaica’s focus on the arts as it positions itself as a cultural force. The donation to the Centre by Frederick, depicts a figure with arms wide open, expressing a searching movement for unification, peace and positivism. '
        'The work transmits a youthful energy and a welcoming of the future.',

    screenshots: [
      'assets/images/embrace0.jpg',
    ],
  ),

  //Untitled
  Sculpture(
    imageUrl: 'assets/images/Untitled_boy3.jpg',
    title: 'Untitled',
    faculty: 'FELS',
    artist: 'Kay Sullivan',
    country: "Jamaican",
    medium: "Resin Bronze",
    description:
    'The sculpture depicts two students, male & female deep in study. The base of it has a series of concentric circles that radiate outward, signifying the ever increasing spread of knowledge- whether by correspondence courses or by the students who eventually become teachers and go on to educate others. '
    'Kay has also made a personal donation of a small sculpture ‘A world of his Own’, (in edition) to the Sculpture Park. It represents a child watching the educated and older people walk by with the hope that when he is of age he will be like them.',

    screenshots: [
      'assets/images/Untitled_boy0.jpg',
      'assets/images/Untitled_boy1.jpg',
      'assets/images/Untitled_boy2.jpg',
    ],
  ),

  //40th Anniversary Logo
  Sculpture(
    imageUrl: 'assets/images/anniversary.jpg',
    videoUrl: 'videos/anniversary1.mp4',
    title: '40th Anniversary Logo',
    faculty: 'N/A',
    artist: 'Justin “Ricky” George',
    country: "St. Lucian",
    medium: "Boned Bronze",
    description:
    'This sculpture is a life sized figure of UTech, Jamaica’s 40th Anniversary logo. It is a symbolic figure, reaching out and upward, with the world at his hand. Under the banner, “Technology driven, development bound”, it heralded the new direction of the University of Technology, Jamaica. '
    'The artist regards this creation as an honour as he sees it contributing to the regional integration of the Caribbean through the medium of art.',

    screenshots: [
      'assets/images/anniversary0.jpg',
      'assets/images/anniversary1.jpg',
      'assets/images/anniversary.jpg',
    ],
  ),

  //The Compass
  Sculpture(
    imageUrl: 'assets/images/compass.jpg',
    title: 'The Compass',
    faculty: 'FOBE',
    artist: 'Basil Watson',
    country: "Jamaican",
    medium: "Steel",
    description:
    'The sculpture depicts Man shaping his environment with the use of technology. The compass is a simple instrument used for describing circles, here- the circle of life, man’s environment and his existence. The head is turned to survey; the feet are firmly planted; the hands are clutching the compass harnessed across his shoulders; the man twists in a dynamic circular motion and provides the intelligence, power and motive force to create the circle of his life. '
    'The compass has another function- with the feet placed east & west and the points of the compass placed north and south, the compass becomes a sundial, charting the daily rise and fall of the sun, the passage of time within the circle of life. Additionally, the linear quality of the work is reminiscent of designing and construction.',

    screenshots: [
      'assets/images/compass0.jpg',
      'assets/images/compass1.jpg',
    ],
  ),

  //Recycled Organism
  Sculpture(
    imageUrl: 'assets/images/organism.jpg',
    title: 'Recycled Organism',
    faculty: 'N/A',
    artist: 'Warren Buckle',
    country: "Jamaican",
    medium: "Steel",
    description:
    'As humans we are constantly improving our lives through developing technology. The sculpture is a metaphor for technological advancement, as it presents the viewer with an artificial structure composed of organic forms and shapes.',
    screenshots: [
      'assets/images/organism0.jpg',
    ],
  ),

  //Totem Pole (Spirit of Fire)
  Sculpture(
    imageUrl: 'assets/images/Totem.jpg',
    title: 'Totem Pole (Spirit of Fire)',
    faculty: 'N/A',
    artist: 'Patrick Mazola "Mazola Wa Mwashigadi"',
    country: "Keyan",
    medium: "Mixed Media",
    description:
    'This sculpture represents a totem of heritage. It has been specially built so that it can be lit thus the reason for its name, ‘spirit of fire’. The fire being referred to by the artist is knowledge. It is intended to burn out illiteracy. Much knowledge has been handed down to us through our ancestors and the artist admonishes us through this work to treasure this heritage. '
    'The sculpture has been placed on the outskirts of the Park as it is one of two pieces not representing a Caribbean region. It represents Africa, the mother continent. So while it is separated from the other pieces, the connection remains as the Caribbean is an extension of Africa.',

    screenshots: [
      'assets/images/Totem.jpg',
    ],
  ),

  //he Bob Marley Sculpture
  Sculpture(
    imageUrl: 'assets/images/bob0.jpg',
    title: 'The Bob Marley Sculpture',
    faculty: 'N/A',
    artist: 'Gregory Pototsky',
    country: "Russian",
    medium: "Steel",
    description:
    'In the words of the artist Gregory Pototsky……Before I started working on the image I had a great spiritual work. I was trying to understand the person and to see how the image should look like. What is a piece of art fully reflecting the idea itself? In the end the idea came to me. '
    'There is everything. Bob Marley, great religious romantic and poet, believed to be Saint. Bob Marley is one of the greatest people of humanity. He is the person who stopped a civil war and found a way out of that greatest dead end, saved people from the abyss between black and white. The crime against black people is horrible and beyond understanding. One of Bob Marley\'s songs had the following lines: "We, black, went Christ\'s way to build the world of love and forgiveness" It is a wonders insight, divine inspiration that determined the development of the future in the monument - civil war, cry for freedom and peace.',
    screenshots: [
      'assets/images/bob.jpg',
    ],
  ),


];

//final List<String> labels = [
//  'Discover',
//  'faculty',
//  'Specials',
//  'New',
//];

//final List<String> myList = [
//  'assets/images/shigatsu_wa_kimi_no_uso.jpg',
//  'assets/images/plastic_memories.png',
//  'assets/images/erased.jpg',
//  'assets/images/seven_deadly_sins.jpg',
//  'assets/images/cobra_kai.jpg',
//];

//final List<String> popular = [
//  'assets/images/stranger_things.jpg',
//  'assets/images/endgame.jpg',
//  'assets/images/oitnb.jpg',
//  'assets/images/daredevil.jpg',
//];
